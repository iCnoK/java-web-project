package com.icnok.WebStoreAPI.controller;

import com.icnok.WebStoreAPI.model.dto.OrderDto;
import com.icnok.WebStoreAPI.model.entity.User;
import com.icnok.WebStoreAPI.service.OrderService;
import com.icnok.WebStoreAPI.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/orders")
public class OrderController {
    private final OrderService orderService;
    private final UserService userService;
    private final static Logger logger = Logger.getLogger(ProductController.class);

    public OrderController(OrderService orderService, UserService userService) {
        this.orderService = orderService;
        this.userService = userService;
    }

    /**
     * create new order (admin)
     *
     * @param orderDto
     */
    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public void createForAdmin(@RequestBody OrderDto orderDto) {
        orderService.create(orderDto);
        logger.info("create order " + orderDto);
    }

    /**
     * get order by id (admin)
     *
     * @param id
     * @return
     */
    @GetMapping(value = "/{id}")
    public ResponseEntity<OrderDto> getByIdForAdmin(@PathVariable Long id) {
        OrderDto result = orderService.getById(id);
        logger.info("return " + result + " order by id " + id);
        return ResponseEntity.ok(result);
    }

    /**
     * get all orders from database (admin)
     *
     * @return
     */
    @GetMapping("/all")
    public ResponseEntity<List<OrderDto>> getAllOrdersForAdmin() {
        List<OrderDto> orderDtoList = orderService.getAll();
        logger.info("return " + orderDtoList.size() + " orders");
        return ResponseEntity.ok(orderDtoList);
    }

    /**
     * update order (admin)
     *
     * @param orderDto
     */
    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public void updateForAdmin(@RequestBody OrderDto orderDto) {
        orderService.update(orderDto);
        logger.info("update " + orderDto + " order");
    }

    /**
     * delete order by id (admin)
     *
     * @param id
     */
    @DeleteMapping(value = "/delete/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteForAdmin(@PathVariable Long id) {
        orderService.deleteById(id);
        logger.info("order with id " + id + " successfully deleted");
    }

    /**
     * create new order
     *
     * @param orderDto
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createForUser(@RequestBody OrderDto orderDto, Authentication authentication) throws Exception {
        String username = authentication.getName();
        User user = userService.findUserByUsername(username);
        if (orderDto.getUserId() == user.getId()) {
            orderService.create(orderDto);
            logger.info("create order " + orderDto);
        } else {
            logger.warn("attempt to create an order for another user by " + user.getUsername() + " user");
            throw new Exception("attempt to create an order for another user");
        }
    }

    /**
     * get all orders of user
     *
     * @param authentication
     * @return
     */
    @GetMapping
    public ResponseEntity<List<OrderDto>> getOrdersOfUser(Authentication authentication) {
        String username = authentication.getName();
        User user = userService.findUserByUsername(username);
        List<OrderDto> orders = orderService.getOrdersByUserId(user.getId());
        logger.info("return " + orders.size() + " orders");
        return ResponseEntity.ok(orders);
    }

    /**
     * delete order by id
     *
     * @param id
     */
    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteForUser(@PathVariable Long id, Authentication authentication) {
        String username = authentication.getName();
        User user = userService.findUserByUsername(username);
        OrderDto orderDto = orderService.getById(id);
        List<OrderDto> orders = orderService.getOrdersByUserId(user.getId());
        if (orders.contains(orderDto)) {
            orderService.deleteById(id);
            logger.info("order with id " + id + " successfully deleted");
        } else {
            logger.warn("an attempt to delete someone else’s order by the user " + user.getUsername() +
                    ", order with id " + id);
        }
    }
}
