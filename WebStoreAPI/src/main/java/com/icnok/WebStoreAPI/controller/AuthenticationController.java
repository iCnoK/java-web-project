package com.icnok.WebStoreAPI.controller;

import com.icnok.WebStoreAPI.model.dto.RegistrationRequestDto;
import com.icnok.WebStoreAPI.model.entity.Status;
import com.icnok.WebStoreAPI.model.entity.User;
import com.icnok.WebStoreAPI.security.AuthRequest;
import com.icnok.WebStoreAPI.security.JWTUtil;
import com.icnok.WebStoreAPI.service.OrderService;
import com.icnok.WebStoreAPI.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/auth")
public class AuthenticationController {

    private final static Logger logger = Logger.getLogger(ProductController.class);

    private final UserService userService;
    private final OrderService orderService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JWTUtil jwtTokenUtil;

    public AuthenticationController(UserService userService, OrderService orderService) {
        this.userService = userService;
        this.orderService = orderService;
    }

    /**
     * login method
     *
     * @param authRequest
     * @return
     */
    @PostMapping("/login")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity login(@RequestBody AuthRequest authRequest) {
        Authentication authentication;
        try {
            authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authRequest.getUsername(), authRequest.getPassword()));
            User user = userService.findUserByUsername(authRequest.getUsername());
            if (user == null) {
                throw new UsernameNotFoundException("User with username: " + authRequest.getUsername() + " not found");
            }
            if (user.getStatus().equals(Status.BANNED.toString())) {
                return ResponseEntity.ok("User was banned");
            }
            final String token = jwtTokenUtil.generateToken((UserDetails) authentication.getPrincipal());

            Map<Object, Object> response = new HashMap<>();
            response.put("id", user.getId());
            response.put("username", authRequest.getUsername());
            response.put("email", user.getEmail());
            response.put("role", user.getRole());
            response.put("status", user.getStatus());
            response.put("token", token);
            response.put("orderslist", orderService.getOrdersByUserId(user.getId()));
            logger.info(authRequest.getUsername() + " login successful");
            return ResponseEntity.ok(response);

        } catch (BadCredentialsException e) {
            logger.warn(authRequest.getUsername() + " login failed");
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid username or password", e);
        }
    }

    /**
     * register method
     *
     * @param registrationRequestDto
     */
    @PostMapping("/registration")
    @ResponseStatus(HttpStatus.OK)
    public void registration(@RequestBody RegistrationRequestDto registrationRequestDto) {
        userService.register(registrationRequestDto, "USER");
        logger.info(registrationRequestDto.getUsername() + " user was successfully registered");
    }
}
