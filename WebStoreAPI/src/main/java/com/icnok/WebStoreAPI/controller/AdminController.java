package com.icnok.WebStoreAPI.controller;

import com.icnok.WebStoreAPI.model.dto.OrderDto;
import com.icnok.WebStoreAPI.model.dto.ProductDto;
import com.icnok.WebStoreAPI.model.dto.RegistrationRequestDto;
import com.icnok.WebStoreAPI.model.dto.UserDto;
import com.icnok.WebStoreAPI.model.entity.Status;
import com.icnok.WebStoreAPI.service.OrderService;
import com.icnok.WebStoreAPI.service.ProductService;
import com.icnok.WebStoreAPI.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/api/admin")
public class AdminController {
    private final UserService userService;
    private final OrderService orderService;
    private final ProductService productService;

    public AdminController(UserService userService, OrderService orderService, ProductService productService) {
        this.userService = userService;
        this.orderService = orderService;
        this.productService = productService;
    }

    /**
     * block user
     *
     * @param userDto
     * @return
     */
    @PostMapping("/ban")
    public ResponseEntity<UserDto> blockUser(@RequestBody UserDto userDto) {
        userDto.setStatus(Status.BANNED.toString());
        userService.update(userDto);
        return ResponseEntity.ok(userDto);
    }

    /**
     * unlock user
     *
     * @param userDto
     * @return
     */
    @PostMapping("/unlock")
    public ResponseEntity<UserDto> unlockUser(@RequestBody UserDto userDto) {
        userDto.setStatus(Status.ACTIVE.toString());
        userService.update(userDto);
        return ResponseEntity.ok(userDto);
    }

    /**
     * fill database with test entities
     */
    @GetMapping("/fill-db")
    public void fillDatabase() {
        var users = createUsers();
        var products = createProducts();
        for (RegistrationRequestDto user : users) {
            userService.register(user, user.getRole());
        }
        for (ProductDto product : products) {
            productService.create(product);
        }
        var orders = createOrders();
        for (OrderDto order : orders) {
            orderService.create(order);
        }
    }

    private List<RegistrationRequestDto> createUsers() {
        List<RegistrationRequestDto> users = new ArrayList<>();
        RegistrationRequestDto adminUser = new RegistrationRequestDto();
        adminUser.setUsername("admin");
        adminUser.setEmail("admin@mail.ru");
        adminUser.setStatus(Status.ACTIVE.toString());
        adminUser.setRole("ADMIN");
        adminUser.setPassword("admin");
        users.add(adminUser);
        RegistrationRequestDto firstUser = new RegistrationRequestDto();
        firstUser.setUsername("user");
        firstUser.setEmail("user@mail.ru");
        firstUser.setStatus(Status.ACTIVE.toString());
        firstUser.setRole("USER");
        firstUser.setPassword("user");
        users.add(firstUser);
        RegistrationRequestDto secondUser = new RegistrationRequestDto();
        secondUser.setUsername("second-user");
        secondUser.setEmail("second-user@mail.ru");
        secondUser.setStatus(Status.ACTIVE.toString());
        secondUser.setRole("USER");
        secondUser.setPassword("second-user");
        users.add(secondUser);
        return users;
    }

    private List<ProductDto> createProducts() {
        List<ProductDto> productDtoList = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            ProductDto productDto = new ProductDto();
            productDto.setName("Name_" + LocalDateTime.now().getNano());
            productDto.setDescription("Description_" + LocalDateTime.now().getNano());
            productDto.setImagePath("ImagePath_" + LocalDateTime.now().getNano());
            productDto.setPrice(1000 * i);
            productDtoList.add(productDto);
        }
        return productDtoList;
    }

    private List<OrderDto> createOrders() {
        List<OrderDto> orderDtoList = new ArrayList<>();
        var products = productService.getAll();
        var users = userService.getAll();
        for (int i = 0; i < products.size(); i++) {
            OrderDto orderDto = new OrderDto();
            orderDto.setProductId(products.get(i).getId());
            if (i % 2 == 0) {
                orderDto.setUserId(users.get(0).getId());
            } else {
                orderDto.setUserId(users.get(1).getId());
            }
            orderDto.setIsPaid(i % 2 == 0);
            orderDtoList.add(orderDto);
        }
        return orderDtoList;
    }
}
