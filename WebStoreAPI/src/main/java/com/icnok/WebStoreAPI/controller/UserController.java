package com.icnok.WebStoreAPI.controller;

import com.icnok.WebStoreAPI.model.dto.RegistrationRequestDto;
import com.icnok.WebStoreAPI.model.dto.UserDto;
import com.icnok.WebStoreAPI.model.dtomapper.impl.UserDtoMapper;
import com.icnok.WebStoreAPI.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {
    private final UserService userService;
    private final UserDtoMapper userDtoMapper;
    private final static Logger logger = Logger.getLogger(ProductController.class);

    public UserController(UserService userService, UserDtoMapper userDtoMapper) {
        this.userService = userService;
        this.userDtoMapper = userDtoMapper;
    }

    /**
     * get all users list
     *
     * @return the list
     */
    @GetMapping()
    public List<UserDto> getAllUsers() {
        var result = userService.getAll();
        logger.info("returned " + result.size() + " users");
        return result;
    }

    /**
     * gets users by id
     *
     * @param userId the user id
     * @return the users by id
     * @throws ResourceNotFoundException the resource not found exception
     */
    @GetMapping("/{id}")
    public ResponseEntity<UserDto> getUsersById(@PathVariable(value = "id") Long userId)
            throws ResourceNotFoundException {
        UserDto user = userService.getById(userId);
        logger.info("returned user by id " + user.getId());
        return ResponseEntity.ok(user);
    }

    /**
     * create user user
     *
     * @param user the user
     * @return the user
     */
    @PostMapping()
    public ResponseEntity<UserDto> createUser(@Valid @RequestBody RegistrationRequestDto user) {
        UserDto userDto = userDtoMapper.mapToDto(userService.register(user, user.getRole()));
        logger.info("user with username " + userDto.getUsername() + " successfully created");
        return ResponseEntity.created(null).body(userDto);
    }

    /**
     * update user response entity
     *
     * @param userId      the user id
     * @param userDetails the user details
     * @return the response entity
     * @throws ResourceNotFoundException the resource not found exception
     */
    @PutMapping("/{id}")
    public ResponseEntity<UserDto> updateUser(
            @PathVariable(value = "id") Long userId, @Valid @RequestBody UserDto userDetails)
            throws ResourceNotFoundException {
        UserDto user = userService.getById(userId);
        user.setStatus(userDetails.getStatus());
        user.setRole(userDetails.getRole());
        user.setEmail(userDetails.getEmail());
        user.setUsername(userDetails.getUsername());
        userService.update(user);
        logger.info("user with id " + user.getId() + " successfully updated");
        return ResponseEntity.ok(user);
    }

    /**
     * delete user map
     *
     * @param userId the user id
     * @return the map
     * @throws Exception the exception
     */
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteUser(@PathVariable(value = "id") Long userId) {
        userService.deleteById(userId);
        logger.info("user with id " + userId + " successfully deleted");
    }
}

