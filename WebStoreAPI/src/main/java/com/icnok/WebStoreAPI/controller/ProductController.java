package com.icnok.WebStoreAPI.controller;

import com.icnok.WebStoreAPI.model.dto.ProductDto;
import com.icnok.WebStoreAPI.service.ProductService;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/products")
public class ProductController {
    private final ProductService productService;
    private final static Logger logger = Logger.getLogger(ProductController.class);

    public ProductController(ProductService productService) {
        this.productService = productService;
    }


    /**
     * create new product
     *
     * @param productDto
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody ProductDto productDto) {
        productService.create(productDto);
        logger.info("create product " + productDto);
    }

    /**
     * get product by id
     *
     * @param id
     * @return
     */
    @GetMapping(value = "/{id}")
    public ResponseEntity<ProductDto> getById(@PathVariable Long id) {
        ProductDto result = productService.getById(id);
        logger.info("return " + result + " product by id " + id);
        return ResponseEntity.ok(result);
    }

    /**
     * get all products from database
     *
     * @return
     */
    @GetMapping
    public ResponseEntity<List<ProductDto>> getAllProducts() {
        List<ProductDto> productDtoList = productService.getAll();
        logger.info("return " + productDtoList.size() + " products");
        return ResponseEntity.ok(productDtoList);
    }

    /**
     * update product
     *
     * @param productDto
     */
    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public void update(@RequestBody ProductDto productDto) {
        productService.update(productDto);
        logger.info("update " + productDto + " product");
    }

    /**
     * delete product by id
     *
     * @param id
     */
    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable Long id) {
        productService.deleteById(id);
        logger.info("product with id " + id + " successfully deleted");
    }
}
