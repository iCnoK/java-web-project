package com.icnok.WebStoreAPI.model.dtomapper.impl;

import com.icnok.WebStoreAPI.model.dto.OrderDto;
import com.icnok.WebStoreAPI.model.dtomapper.DtoMapper;
import com.icnok.WebStoreAPI.model.entity.Order;
import org.springframework.stereotype.Component;

@Component
public class OrderDtoMapper implements DtoMapper<Order, OrderDto> {
    @Override
    public OrderDto mapToDto(Order entity) {
        if (entity == null) {
            return null;
        }
        OrderDto orderDto = new OrderDto();
        orderDto.setId(entity.getId());
        orderDto.setProductId(entity.getProductId());
        orderDto.setUserId(entity.getUserId());
        orderDto.setIsPaid(entity.getIsPaid());
        return orderDto;
    }

    @Override
    public Order mapToEntity(OrderDto dto) {
        if (dto == null) {
            return null;
        }
        Order order = new Order();
        order.setId(dto.getId());
        order.setProductId(dto.getProductId());
        order.setUserId(dto.getUserId());
        order.setIsPaid(dto.getIsPaid());
        return order;
    }
}
