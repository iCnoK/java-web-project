package com.icnok.WebStoreAPI.model.dtomapper.impl;

import com.icnok.WebStoreAPI.model.dto.UserDto;
import com.icnok.WebStoreAPI.model.dtomapper.DtoMapper;
import com.icnok.WebStoreAPI.model.entity.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class UserDtoMapper implements DtoMapper<User, UserDto> {
    @Override
    public UserDto mapToDto(User entity) {
        if (entity == null) {
            return null;
        }
        UserDto userDto = new UserDto();
        userDto.setId(entity.getId());
        userDto.setEmail(entity.getEmail());
        userDto.setRole(entity.getRole());
        userDto.setUsername(entity.getUsername());
        userDto.setStatus(entity.getStatus());
        userDto.setOrderDtoList(new ArrayList<>());
        return userDto;
    }

    @Override
    public User mapToEntity(UserDto dto) {
        if (dto == null) {
            return null;
        }
        User user = new User();
        user.setId(dto.getId());
        user.setUsername(dto.getUsername());
        user.setEmail(dto.getEmail());
        user.setRole(dto.getRole());
        user.setStatus(dto.getStatus());
        return user;
    }
}
