package com.icnok.WebStoreAPI.model.dtomapper;

import com.icnok.WebStoreAPI.model.dto.BaseDto;
import com.icnok.WebStoreAPI.model.entity.BaseEntity;

public interface DtoMapper<Entity extends BaseEntity, Dto extends BaseDto> {
    Dto mapToDto(Entity entity);

    Entity mapToEntity(Dto dto);
}
