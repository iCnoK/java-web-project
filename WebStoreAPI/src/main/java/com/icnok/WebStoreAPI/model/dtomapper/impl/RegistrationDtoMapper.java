package com.icnok.WebStoreAPI.model.dtomapper.impl;

import com.icnok.WebStoreAPI.model.dto.RegistrationRequestDto;
import com.icnok.WebStoreAPI.model.dto.UserDto;
import com.icnok.WebStoreAPI.model.entity.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class RegistrationDtoMapper extends UserDtoMapper {
    @Override
    public RegistrationRequestDto mapToDto(User user) {
        if (user == null) {
            return null;
        }
        UserDto userDto = super.mapToDto(user);
        RegistrationRequestDto requestDto = new RegistrationRequestDto();
        requestDto.setId(userDto.getId());
        requestDto.setUsername(userDto.getUsername());
        requestDto.setPassword(user.getPassword());
        requestDto.setRole(userDto.getRole());
        requestDto.setStatus(userDto.getStatus());
        requestDto.setEmail(userDto.getEmail());
        requestDto.setOrderDtoList(userDto.getOrderDtoList());
        return requestDto;
    }

    public User mapToEntity(RegistrationRequestDto registrationRequestDto) {
        if (registrationRequestDto == null) {
            return null;
        }
        User user = super.mapToEntity(registrationRequestDto);
        user.setPassword(registrationRequestDto.getPassword());
        return user;
    }
}
