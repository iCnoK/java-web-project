package com.icnok.WebStoreAPI.model.entity;

public enum Status {
    ACTIVE,
    BANNED
}
