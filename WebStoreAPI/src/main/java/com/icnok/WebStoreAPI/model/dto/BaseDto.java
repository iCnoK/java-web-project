package com.icnok.WebStoreAPI.model.dto;

import java.util.Objects;

public abstract class BaseDto {
    protected long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BaseDto baseDto = (BaseDto) o;
        return id == baseDto.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
