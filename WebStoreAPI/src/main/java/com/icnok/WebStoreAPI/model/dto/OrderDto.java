package com.icnok.WebStoreAPI.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderDto extends BaseDto {
    private long productId;
    private long userId;
    private boolean ispaid;

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public boolean getIsPaid() {
        return ispaid;
    }

    public void setIsPaid(boolean ispaid) {
        this.ispaid = ispaid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderDto orderDto = (OrderDto) o;
        return productId == orderDto.productId &&
                userId == orderDto.userId &&
                ispaid == orderDto.ispaid;
    }

    @Override
    public int hashCode() {
        return Objects.hash(productId, userId, ispaid);
    }
}
