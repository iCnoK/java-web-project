package com.icnok.WebStoreAPI.model.entity;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "orders")
@EntityListeners(AuditingEntityListener.class)
public class Order extends BaseEntity {
    @Column(name = "product_id", nullable = false)
    private long productId;

    @Column(name = "user_id", nullable = false)
    private long userId;

    @Column(name = "is_paid", nullable = false)
    private boolean isPaid;

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public boolean getIsPaid() {
        return isPaid;
    }

    public void setIsPaid(boolean paid) {
        isPaid = paid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return productId == order.productId &&
                userId == order.userId &&
                isPaid == order.isPaid;
    }

    @Override
    public int hashCode() {
        return Objects.hash(productId, userId, isPaid);
    }
}
