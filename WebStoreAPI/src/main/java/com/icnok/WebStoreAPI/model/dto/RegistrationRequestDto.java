package com.icnok.WebStoreAPI.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RegistrationRequestDto extends UserDto {
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RegistrationRequestDto that = (RegistrationRequestDto) o;
        return password.equals(that.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(password);
    }
}
