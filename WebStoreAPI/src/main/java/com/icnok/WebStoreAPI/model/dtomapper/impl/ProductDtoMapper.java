package com.icnok.WebStoreAPI.model.dtomapper.impl;

import com.icnok.WebStoreAPI.model.dto.ProductDto;
import com.icnok.WebStoreAPI.model.dtomapper.DtoMapper;
import com.icnok.WebStoreAPI.model.entity.Product;
import org.springframework.stereotype.Component;

@Component
public class ProductDtoMapper implements DtoMapper<Product, ProductDto> {
    @Override
    public ProductDto mapToDto(Product entity) {
        if (entity == null) {
            return null;
        }
        ProductDto productDto = new ProductDto();
        productDto.setId(entity.getId());
        productDto.setPrice(entity.getPrice());
        productDto.setName(entity.getName());
        productDto.setDescription(entity.getDescription());
        productDto.setImagePath(entity.getImagePath());
        return productDto;
    }

    @Override
    public Product mapToEntity(ProductDto dto) {
        if (dto == null) {
            return null;
        }
        Product product = new Product();
        product.setId(dto.getId());
        product.setPrice(dto.getPrice());
        product.setName(dto.getName());
        product.setDescription(dto.getDescription());
        product.setImagePath(dto.getImagePath());
        return product;
    }
}
