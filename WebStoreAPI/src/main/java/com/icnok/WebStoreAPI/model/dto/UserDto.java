package com.icnok.WebStoreAPI.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDto extends BaseDto {
    private String username;
    private String email;
    private String role;
    private String status;
    private List<OrderDto> orderDtoList;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<OrderDto> getOrderDtoList() {
        return orderDtoList;
    }

    public void setOrderDtoList(List<OrderDto> orderDtoList) {
        this.orderDtoList = orderDtoList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDto userDto = (UserDto) o;
        return username.equals(userDto.username) &&
                email.equals(userDto.email) &&
                role.equals(userDto.role) &&
                status.equals(userDto.status) &&
                orderDtoList.equals(userDto.orderDtoList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, email, role, status, orderDtoList);
    }
}
