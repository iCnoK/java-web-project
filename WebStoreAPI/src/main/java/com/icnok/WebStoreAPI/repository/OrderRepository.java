package com.icnok.WebStoreAPI.repository;

import com.icnok.WebStoreAPI.model.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Long> {

}
