package com.icnok.WebStoreAPI.service.impl;

import com.icnok.WebStoreAPI.controller.ProductController;
import com.icnok.WebStoreAPI.model.dto.RegistrationRequestDto;
import com.icnok.WebStoreAPI.model.dto.UserDto;
import com.icnok.WebStoreAPI.model.dtomapper.impl.RegistrationDtoMapper;
import com.icnok.WebStoreAPI.model.dtomapper.impl.UserDtoMapper;
import com.icnok.WebStoreAPI.model.entity.Status;
import com.icnok.WebStoreAPI.model.entity.User;
import com.icnok.WebStoreAPI.repository.UserRepository;
import com.icnok.WebStoreAPI.service.UserService;
import org.apache.log4j.Logger;
import org.hibernate.exception.DataException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private PasswordEncoder passwordEncoder;

    private final UserRepository userRepository;
    private final UserDtoMapper userDtoMapper;
    private final RegistrationDtoMapper registrationDtoMapper;

    private final static Logger logger = Logger.getLogger(ProductController.class);

    public UserServiceImpl(UserRepository userRepository, UserDtoMapper userDtoMapper, RegistrationDtoMapper registrationDtoMapper) {
        this.userRepository = userRepository;
        this.userDtoMapper = userDtoMapper;
        this.registrationDtoMapper = registrationDtoMapper;
    }

    @Override
    public void create(UserDto userDto) {
        User user = userDtoMapper.mapToEntity(userDto);
        try {
            userRepository.save(user);
            logger.info("User with id: " + user.getId() + " save successfully ");
        } catch (DataException e) {
            logger.error(e.getMessage());
        }
    }

    @Override
    public UserDto getById(Long id) {
        User user = userRepository.findById(id).orElse(null);

        if (user == null) {
            logger.warn("User not find by id: " + id);
            return null;
        }
        logger.info("User found by id: " + id);
        return userDtoMapper.mapToDto(user);
    }

    @Override
    public List<UserDto> getAll() {
        List<User> users = userRepository.findAll();
        logger.info(users.size() + " users found");
        return users.stream()
                .map(userDtoMapper::mapToDto)
                .collect(Collectors.toList());
    }

    @Override
    public void update(UserDto userDto) {
        User user = userRepository.findById(userDto.getId()).orElse(null);
        if (user != null) {
            user.setStatus(userDto.getStatus());
            userRepository.save(user);
            logger.info("User with id: " + user.getId() + " successfully updated");
        } else {
            logger.warn("User with id: " + user.getId() + " update failed");
        }
    }

    @Override
    public void deleteById(Long id) {
        try {
            userRepository.deleteById(id);
            logger.info("User with id: " + id + " successfully deleted ");
        } catch (DataException e) {
            logger.info("User with id: " + id + " not exist");
        }
        logger.info("User with id: " + id + " not exist");
    }

    @Override
    public User register(RegistrationRequestDto registrationRequestDto, String role) {
        User user = registrationDtoMapper.mapToEntity(registrationRequestDto);

        user.setPassword(passwordEncoder.encode(registrationRequestDto.getPassword()));
        user.setRole(role);
        user.setStatus(Status.ACTIVE.toString());

        User registeredUser = userRepository.save(user);
        logger.info("User with id: " + registeredUser.getId() + " successfully registered");
        return registeredUser;
    }

    @Override
    public User findUserByUsername(String username) {
        User result = userRepository.findByUsername(username);
        logger.info("User with id: " + result.getId() + " found by username: " + username);
        return result;
    }
}
