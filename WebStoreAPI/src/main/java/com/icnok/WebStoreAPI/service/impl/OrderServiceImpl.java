package com.icnok.WebStoreAPI.service.impl;

import com.icnok.WebStoreAPI.model.dto.OrderDto;
import com.icnok.WebStoreAPI.model.dtomapper.impl.OrderDtoMapper;
import com.icnok.WebStoreAPI.model.entity.Order;
import com.icnok.WebStoreAPI.repository.OrderRepository;
import com.icnok.WebStoreAPI.service.OrderService;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl implements OrderService {
    private final OrderRepository orderRepository;
    private final OrderDtoMapper orderDtoMapper;
    private final static Logger logger = Logger.getLogger(ProductServiceImpl.class);

    public OrderServiceImpl(OrderRepository orderRepository, OrderDtoMapper orderDtoMapper) {
        this.orderRepository = orderRepository;
        this.orderDtoMapper = orderDtoMapper;
    }

    @Override
    public List<OrderDto> getOrdersByUserId(long userId) {
        List<Order> orders = orderRepository.findAll();
        logger.info("find " + orders.size() + " orders by user id " + userId);
        return orders
                .stream()
                .filter(e -> e.getUserId() == userId)
                .map(orderDtoMapper::mapToDto)
                .collect(Collectors.toList());
    }

    @Override
    public void create(OrderDto orderDto) {
        Order order = orderDtoMapper.mapToEntity(orderDto);
        try {
            orderRepository.save(order);
            logger.info("order with id " + order.getId() + " successfully created");
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    @Override
    public OrderDto getById(Long id) {
        Order order = orderRepository.findById(id).orElse(null);
        if (order == null) {
            logger.warn("order with id " + id + " not found");
            return null;
        }
        logger.info("order with id " + id + " found");
        return orderDtoMapper.mapToDto(order);
    }

    @Override
    public List<OrderDto> getAll() {
        List<Order> orders = orderRepository.findAll();
        logger.info(orders.size() + " orders was found");
        return orders
                .stream()
                .map(orderDtoMapper::mapToDto)
                .collect(Collectors.toList());
    }

    @Override
    public void update(OrderDto orderDto) {
        Order order = orderDtoMapper.mapToEntity(orderDto);
        orderRepository.save(order);
        logger.info("order with id " + " successfully updated");
    }

    @Override
    public void deleteById(Long id) {
        try {
            orderRepository.deleteById(id);
            logger.info("order with id " + id + " successfully deleted");
        } catch (Exception e) {
            logger.info("order with id " + id + " not exists");
        }
    }
}
