package com.icnok.WebStoreAPI.service.impl;

import com.icnok.WebStoreAPI.model.dto.ProductDto;
import com.icnok.WebStoreAPI.model.dtomapper.DtoMapper;
import com.icnok.WebStoreAPI.model.entity.Product;
import com.icnok.WebStoreAPI.repository.ProductRepository;
import com.icnok.WebStoreAPI.service.ProductService;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;
    private final DtoMapper<Product, ProductDto> productDtoMapper;
    private final static Logger logger = Logger.getLogger(ProductServiceImpl.class);

    public ProductServiceImpl(ProductRepository productRepository, DtoMapper<Product, ProductDto> productDtoMapper) {
        this.productRepository = productRepository;
        this.productDtoMapper = productDtoMapper;
    }

    @Override
    public void create(ProductDto productDto) {
        try {
            Product product = productDtoMapper.mapToEntity(productDto);
            productRepository.save(product);
            logger.info("product with id " + product.getId() + " successfully created");
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    @Override
    public ProductDto getById(Long id) {
        Product product = productRepository.findById(id).orElse(null);
        if (product == null) {
            logger.warn("product with id " + id + " not found");
            return null;
        }
        logger.info("product with id " + id + " found");
        return productDtoMapper.mapToDto(product);
    }

    @Override
    public List<ProductDto> getAll() {
        List<Product> products = productRepository.findAll();
        logger.info(products.size() + " products was found");
        return products
                .stream()
                .map(productDtoMapper::mapToDto)
                .collect(Collectors.toList());
    }

    @Override
    public void update(ProductDto productDto) {
        Product product = productDtoMapper.mapToEntity(productDto);
        productRepository.save(product);
        logger.info("product with id " + " successfully updated");
    }

    @Override
    public void deleteById(Long id) {
        try {
            productRepository.deleteById(id);
            logger.info("product with id " + id + " successfully deleted");
        } catch (Exception e) {
            logger.info("product with id " + id + " not exists");
        }
    }
}
