package com.icnok.WebStoreAPI.service;

import com.icnok.WebStoreAPI.model.dto.ProductDto;

public interface ProductService extends GenericService<ProductDto> {

}
