package com.icnok.WebStoreAPI.service;

import com.icnok.WebStoreAPI.model.dto.OrderDto;

import java.util.List;

public interface OrderService extends GenericService<OrderDto> {
    List<OrderDto> getOrdersByUserId(long userId);
}
