package com.icnok.WebStoreAPI.service;

import java.util.List;

public interface GenericService<T> {
    void create(T t);

    T getById(Long id);

    List<T> getAll();

    void update(T t);

    void deleteById(Long id);
}
