package com.icnok.WebStoreAPI.service;

import com.icnok.WebStoreAPI.model.dto.RegistrationRequestDto;
import com.icnok.WebStoreAPI.model.dto.UserDto;
import com.icnok.WebStoreAPI.model.entity.User;

public interface UserService extends GenericService<UserDto> {
    User register(RegistrationRequestDto registrationRequestDto, String role);

    User findUserByUsername(String username);
}
