package com.icnok.WebStoreAPI.model.dtomapper.impl;

import com.icnok.WebStoreAPI.model.dto.UserDto;
import com.icnok.WebStoreAPI.model.entity.Status;
import com.icnok.WebStoreAPI.model.entity.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UserDtoMapperTest {

    private UserDtoMapper userDtoMapper;

    @BeforeEach
    void setUp() {
        userDtoMapper = new UserDtoMapper();
    }

    @Test
    void mapToDto() {
        User user = new User();
        user.setStatus(Status.ACTIVE.toString());
        user.setId(1);
        user.setRole("USER");
        user.setEmail("1");
        user.setUsername("1");
        user.setPassword("1");

        UserDto userDtoTest = new UserDto();
        userDtoTest.setStatus(Status.ACTIVE.toString());
        userDtoTest.setId(1);
        userDtoTest.setRole("USER");
        userDtoTest.setEmail("1");
        userDtoTest.setUsername("1");
        userDtoTest.setOrderDtoList(new ArrayList<>());

        UserDto userDto = userDtoMapper.mapToDto(user);
        assertEquals(userDto, userDtoTest);
    }

    @Test
    void mapToEntity() {
        UserDto userDto = new UserDto();
        userDto.setStatus(Status.ACTIVE.toString());
        userDto.setId(1);
        userDto.setRole("USER");
        userDto.setEmail("1");
        userDto.setUsername("1");
        userDto.setOrderDtoList(new ArrayList<>());

        User userTest = new User();
        userTest.setStatus(Status.ACTIVE.toString());
        userTest.setId(1);
        userTest.setRole("USER");
        userTest.setEmail("1");
        userTest.setUsername("1");
        userTest.setPassword("1");

        User user = userDtoMapper.mapToEntity(userDto);
        user.setPassword("1");
        assertEquals(user, userTest);
    }
}