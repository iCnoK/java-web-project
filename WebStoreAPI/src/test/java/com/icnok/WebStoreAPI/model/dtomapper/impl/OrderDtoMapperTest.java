package com.icnok.WebStoreAPI.model.dtomapper.impl;

import com.icnok.WebStoreAPI.model.dto.OrderDto;
import com.icnok.WebStoreAPI.model.entity.Order;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class OrderDtoMapperTest {

    private OrderDtoMapper orderDtoMapper;

    @BeforeEach
    void setUp() {
        orderDtoMapper = new OrderDtoMapper();
    }

    @Test
    void mapToDto() {
        Order order = new Order();
        order.setUserId(1);
        order.setProductId(1);
        order.setIsPaid(true);
        order.setId(1);

        OrderDto orderDto = new OrderDto();
        orderDto.setUserId(1);
        orderDto.setProductId(1);
        orderDto.setIsPaid(true);
        orderDto.setId(1);

        OrderDto orderDtoTest = orderDtoMapper.mapToDto(order);
        assertEquals(orderDto, orderDtoTest);
    }

    @Test
    void mapToEntity() {
        OrderDto orderDto = new OrderDto();
        orderDto.setUserId(1);
        orderDto.setProductId(1);
        orderDto.setIsPaid(true);
        orderDto.setId(1);

        Order orderTest = new Order();
        orderTest.setUserId(1);
        orderTest.setProductId(1);
        orderTest.setIsPaid(true);
        orderTest.setId(1);

        Order order = orderDtoMapper.mapToEntity(orderDto);
        assertEquals(order, orderTest);
    }
}