package com.icnok.WebStoreAPI.model.dtomapper.impl;

import com.icnok.WebStoreAPI.model.dto.RegistrationRequestDto;
import com.icnok.WebStoreAPI.model.entity.Status;
import com.icnok.WebStoreAPI.model.entity.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RegistrationDtoMapperTest {

    private RegistrationDtoMapper registrationDtoMapper;

    @BeforeEach
    void setUp() {
        registrationDtoMapper = new RegistrationDtoMapper();
    }

    @Test
    void mapToDto() {
        User user = new User();
        user.setStatus(Status.ACTIVE.toString());
        user.setId(1);
        user.setRole("USER");
        user.setEmail("1");
        user.setUsername("1");
        user.setPassword("1");

        RegistrationRequestDto requestDtoTest = new RegistrationRequestDto();
        requestDtoTest.setStatus(Status.ACTIVE.toString());
        requestDtoTest.setId(1);
        requestDtoTest.setRole("USER");
        requestDtoTest.setEmail("1");
        requestDtoTest.setUsername("1");
        requestDtoTest.setPassword("1");
        requestDtoTest.setOrderDtoList(new ArrayList<>());

        RegistrationRequestDto requestDto = registrationDtoMapper.mapToDto(user);
        assertEquals(requestDto, requestDtoTest);
    }

    @Test
    void mapToEntity() {
        RegistrationRequestDto requestDto = new RegistrationRequestDto();
        requestDto.setStatus(Status.ACTIVE.toString());
        requestDto.setId(1);
        requestDto.setRole("USER");
        requestDto.setEmail("1");
        requestDto.setUsername("1");
        requestDto.setPassword("1");
        requestDto.setOrderDtoList(new ArrayList<>());

        User userTest = new User();
        userTest.setStatus(Status.ACTIVE.toString());
        userTest.setId(1);
        userTest.setRole("USER");
        userTest.setEmail("1");
        userTest.setUsername("1");
        userTest.setPassword("1");

        User user = registrationDtoMapper.mapToEntity(requestDto);
        assertEquals(user, userTest);
    }
}