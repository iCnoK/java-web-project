package com.icnok.WebStoreAPI.model.dtomapper.impl;

import com.icnok.WebStoreAPI.model.dto.ProductDto;
import com.icnok.WebStoreAPI.model.entity.Product;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ProductDtoMapperTest {

    private ProductDtoMapper productDtoMapper;

    @BeforeEach
    void setUp() {
        productDtoMapper = new ProductDtoMapper();
    }

    @Test
    void mapToDto() {
        Product product = new Product();
        product.setId(1);
        product.setName("1");
        product.setDescription("1");
        product.setImagePath("1");
        product.setPrice(1);

        ProductDto productDtoTest = new ProductDto();
        productDtoTest.setId(1);
        productDtoTest.setName("1");
        productDtoTest.setDescription("1");
        productDtoTest.setImagePath("1");
        productDtoTest.setPrice(1);

        ProductDto productDto = productDtoMapper.mapToDto(product);
        assertEquals(productDto, productDtoTest);
    }

    @Test
    void mapToEntity() {
        ProductDto productDto = new ProductDto();
        productDto.setId(1);
        productDto.setName("1");
        productDto.setDescription("1");
        productDto.setImagePath("1");
        productDto.setPrice(1);

        Product productTest = new Product();
        productTest.setId(1);
        productTest.setName("1");
        productTest.setDescription("1");
        productTest.setImagePath("1");
        productTest.setPrice(1);

        Product product = productDtoMapper.mapToEntity(productDto);
        assertEquals(product, productTest);
    }

}