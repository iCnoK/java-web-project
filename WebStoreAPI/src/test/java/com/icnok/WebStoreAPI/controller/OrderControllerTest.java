package com.icnok.WebStoreAPI.controller;

import com.icnok.WebStoreAPI.AuxiliaryFunctions;
import com.icnok.WebStoreAPI.model.dto.OrderDto;
import com.icnok.WebStoreAPI.model.dto.ProductDto;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
class OrderControllerTest extends AuxiliaryFunctions {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void createForAdmin() throws Exception {
        String uri = "/api/orders/create";
        OrderDto orderDto = new OrderDto();

        var auth = auth("admin", "admin");
        var products = getProducts();

        orderDto.setUserId((Integer) auth.get("id"));
        orderDto.setProductId(products.get(0).getId());

        String orderDtoJson = mapToJson(orderDto);

        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(orderDtoJson)
                .header("Authorization", "Bearer_" + auth.get("token")))
                .andDo(print())
                .andExpect(status().isCreated());
    }

    @Test
    void getByIdForAdmin() throws Exception {
        var auth = auth("admin", "admin");
        var orders = mapFromJson(mapToJson(auth.get("orderslist")), OrderDto[].class);

        if (orders.length > 0) {
            String uri = "/api/orders/" + orders[0].getId();

            mockMvc.perform(MockMvcRequestBuilders.get(uri)
                    .header("Authorization", "Bearer_" + auth.get("token")))
                    .andDo(print())
                    .andExpect(status().isOk());
        }
    }

    @Test
    void getAllOrdersForAdmin() throws Exception {
        String uri = "/api/orders/all";
        var auth = auth("admin", "admin");
        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                .header("Authorization", "Bearer_" + auth.get("token")))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void updateForAdmin() throws Exception {
        String uri = "/api/orders";
        var auth = auth("admin", "admin");

        List<OrderDto> orderDtoList = getOrders("/api/orders/all", (String) auth.get("token"));
        if (orderDtoList.size() > 0) {
            mockMvc.perform(MockMvcRequestBuilders.put(uri)
                    .header("Authorization", "Bearer_" + auth.get("token"))
                    .contentType(MediaType.APPLICATION_JSON).content(mapToJson(orderDtoList.get(0))))
                    .andDo(print())
                    .andExpect(status().isOk());
        }
    }

    @Test
    void deleteForAdmin() throws Exception {
        var auth = auth("admin", "admin");
        List<OrderDto> orderDtoList = getOrders("/api/orders/all", (String) auth.get("token"));
        if (orderDtoList.size() > 0) {
            String uri = "/api/orders/delete/" + orderDtoList.get(0).getId();
            mockMvc.perform(MockMvcRequestBuilders.delete(uri)
                    .header("Authorization", "Bearer_" + auth.get("token")))
                    .andDo(print())
                    .andExpect(status().isOk());
        }
    }

    @Test
    void createForUser() throws Exception {
        String uri = "/api/orders";
        var auth = auth("user", "user");
        OrderDto orderDto = new OrderDto();
        List<ProductDto> products = getProducts();
        if (products.size() > 0) {
            orderDto.setUserId((Integer) auth.get("id"));
            orderDto.setProductId(products.get(0).getId());
            orderDto.setIsPaid(true);

            mockMvc.perform(MockMvcRequestBuilders.post(uri)
                    .header("Authorization", "Bearer_" + auth.get("token"))
                    .contentType(MediaType.APPLICATION_JSON).content(mapToJson(orderDto)))
                    .andDo(print())
                    .andExpect(status().isCreated());
        }
    }

    @Test
    void getOrdersOfUser() throws Exception {
        String uri = "/api/orders";
        var auth = auth("user", "user");
        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                .header("Authorization", "Bearer_" + auth.get("token")))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void deleteForUser() throws Exception {
        var auth = auth("user", "user");
        var orders = getOrders("/api/orders", (String) auth.get("token"));
        if (orders.size() > 0) {
            String uri = "/api/orders/" + orders.get(0).getId();
            mockMvc.perform(MockMvcRequestBuilders.delete(uri)
                    .header("Authorization", "Bearer_" + auth.get("token")))
                    .andDo(print())
                    .andExpect(status().isOk());
        }
    }
}