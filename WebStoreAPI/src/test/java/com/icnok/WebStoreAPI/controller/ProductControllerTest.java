package com.icnok.WebStoreAPI.controller;

import com.icnok.WebStoreAPI.AuxiliaryFunctions;
import com.icnok.WebStoreAPI.model.dto.ProductDto;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
class ProductControllerTest extends AuxiliaryFunctions {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void create() throws Exception {
        String uri = "/api/products";
        var auth = auth("admin", "admin");

        ProductDto productDto = new ProductDto();
        productDto.setName("TestProductN");
        productDto.setDescription("TestProductD");
        productDto.setPrice(10000);
        productDto.setImagePath("IMAGE_PATH");

        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(mapToJson(productDto))
                .header("Authorization", "Bearer_" + auth.get("token")))
                .andDo(print())
                .andExpect(status().isCreated());
    }

    @Test
    void getById() throws Exception {
        var products = getProducts();
        if (products.size() > 0) {
            String uri = "/api/products/" + products.get(0).getId();
            mockMvc.perform(MockMvcRequestBuilders.get(uri))
                    .andDo(print())
                    .andExpect(status().isOk());
        }
    }

    @Test
    void getAllProducts() throws Exception {
        String uri = "/api/products";
        mockMvc.perform(MockMvcRequestBuilders.get(uri))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void update() throws Exception {
        String uri = "/api/products";
        var auth = auth("admin", "admin");
        var products = getProducts();
        if (products.size() > 0) {
            mockMvc.perform(MockMvcRequestBuilders.put(uri)
                    .contentType(MediaType.APPLICATION_JSON).content(mapToJson(products.get(0)))
                    .header("Authorization", "Bearer_" + auth.get("token")))
                    .andDo(print())
                    .andExpect(status().isOk());
        }
    }

    @Test
    void delete() throws Exception {
        var auth = auth("admin", "admin");
        var products = getProducts();
        if (products.size() > 0) {
            String uri = "/api/products/" + products.get(products.size() - 1).getId();
            mockMvc.perform(MockMvcRequestBuilders.delete(uri)
                    .header("Authorization", "Bearer_" + auth.get("token")))
                    .andDo(print())
                    .andExpect(status().isOk());
        }
    }
}