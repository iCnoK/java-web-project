package com.icnok.WebStoreAPI.controller;

import com.icnok.WebStoreAPI.AuxiliaryFunctions;
import com.icnok.WebStoreAPI.model.dto.RegistrationRequestDto;
import com.icnok.WebStoreAPI.model.entity.Status;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
class UserControllerTest extends AuxiliaryFunctions {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void getAllUsers() throws Exception {
        String uri = "/api/users";
        var auth = auth("admin", "admin");
        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                .header("Authorization", "Bearer_" + auth.get("token")))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void getUsersById() throws Exception {
        var auth = auth("admin", "admin");
        var users = getUsers((String) auth.get("token"));
        String uri = "/api/users/" + users.get(0).getId();
        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                .header("Authorization", "Bearer_" + auth.get("token")))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void createUser() throws Exception {
        String uri = "/api/users";
        var auth = auth("admin", "admin");
        RegistrationRequestDto requestDto = new RegistrationRequestDto();
        requestDto.setUsername("SECONDADMIN");
        requestDto.setPassword("SECONDADMIN");
        requestDto.setEmail("SECONDADMIN@mail.ru");
        requestDto.setStatus(Status.ACTIVE.toString());
        requestDto.setRole("ADMIN");

        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .header("Authorization", "Bearer_" + auth.get("token"))
                .contentType(MediaType.APPLICATION_JSON).content(mapToJson(requestDto)))
                .andDo(print())
                .andExpect(status().isCreated());
    }

    @Test
    void updateUser() throws Exception {
        var auth = auth("admin", "admin");
        var users = getUsers((String) auth.get("token"));
        String uri = "/api/users/" + users.get(users.size() - 1).getId();
        mockMvc.perform(MockMvcRequestBuilders.put(uri)
                .contentType(MediaType.APPLICATION_JSON).content(mapToJson(users.get(users.size() - 1)))
                .header("Authorization", "Bearer_" + auth.get("token")))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void deleteUser() throws Exception {
        var auth = auth("admin", "admin");
        var users = getUsers((String) auth.get("token"));
        String uri = "/api/users/" + users.get(users.size() - 1).getId();
        mockMvc.perform(MockMvcRequestBuilders.delete(uri)
                .header("Authorization", "Bearer_" + auth.get("token")))
                .andDo(print())
                .andExpect(status().isOk());
    }
}