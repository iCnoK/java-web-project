package com.icnok.WebStoreAPI.controller;

import com.icnok.WebStoreAPI.AuxiliaryFunctions;
import com.icnok.WebStoreAPI.model.dto.RegistrationRequestDto;
import com.icnok.WebStoreAPI.model.entity.Status;
import com.icnok.WebStoreAPI.security.AuthRequest;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
class AuthenticationControllerTest extends AuxiliaryFunctions {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void login() throws Exception {
        String uri = "/api/auth/login";
        AuthRequest authRequest = new AuthRequest();
        authRequest.setUsername("admin");
        authRequest.setPassword("admin");

        String authRequestJson = mapToJson(authRequest);

        mockMvc.perform(post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(authRequestJson))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void registration() throws Exception {
        String uri = "/api/auth/registration";
        RegistrationRequestDto requestDto = new RegistrationRequestDto();
        requestDto.setUsername("testUser");
        requestDto.setPassword("testUser");
        requestDto.setRole("USER");
        requestDto.setStatus(Status.ACTIVE.toString());
        requestDto.setEmail("testUser" + LocalDateTime.now().getNano() + "@mail.ru");

        String requestDtoJson = mapToJson(requestDto);

        this.mockMvc.perform(post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(requestDtoJson))
                .andDo(print())
                .andExpect(status().isOk());
    }
}