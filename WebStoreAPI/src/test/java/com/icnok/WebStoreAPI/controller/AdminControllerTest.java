package com.icnok.WebStoreAPI.controller;

import com.icnok.WebStoreAPI.AuxiliaryFunctions;
import com.icnok.WebStoreAPI.model.dto.UserDto;
import com.icnok.WebStoreAPI.model.entity.Status;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
class AdminControllerTest extends AuxiliaryFunctions {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void blockUser() throws Exception {
        String uri = "/api/admin/ban";
        String token = (String) auth("admin", "admin").get("token");
        var users = getUsers(token);
        if (users.size() > 0) {
            UserDto userDto = users.get(2);
            String userDtoJson = mapToJson(userDto);
            this.mockMvc.perform(post(uri)
                    .content(userDtoJson).contentType(MediaType.APPLICATION_JSON)
                    .header("Authorization", "Bearer_" + token))
                    .andDo(print())
                    .andExpect(status().isOk());
        }
    }

    @Test
    void unlockUser() throws Exception {
        String uri = "/api/admin/unlock";
        String token = (String) auth("admin", "admin").get("token");
        var users = getUsers(token);
        if (users.size() > 0) {
            UserDto userDto = users.get(2);
            String userDtoJson = mapToJson(userDto);
            this.mockMvc.perform(post(uri)
                    .content(userDtoJson).contentType(MediaType.APPLICATION_JSON)
                    .header("Authorization", "Bearer_" + token))
                    .andDo(print())
                    .andExpect(status().isOk());
        }
    }
}