package com.icnok.WebStoreAPI;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.icnok.WebStoreAPI.model.dto.OrderDto;
import com.icnok.WebStoreAPI.model.dto.ProductDto;
import com.icnok.WebStoreAPI.model.dto.UserDto;
import com.icnok.WebStoreAPI.security.AuthRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
public abstract class AuxiliaryFunctions {

    @Autowired
    private MockMvc mockMvc;

    protected String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    protected <T> T mapFromJson(String json, Class<T> clazz)
            throws JsonParseException, JsonMappingException, IOException {

        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, clazz);
    }

    protected Map<Object, Object> auth(String username, String password) throws Exception {
        String uri = "/api/auth/login";
        AuthRequest authRequest = new AuthRequest();
        authRequest.setUsername(username);
        authRequest.setPassword(password);

        String authRequestJson = mapToJson(authRequest);

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(authRequestJson))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
        Map<Object, Object> response = new HashMap<>();
        response = mapFromJson(mvcResult.getResponse().getContentAsString(), Map.class);
        return response;
    }

    protected List<ProductDto> getProducts() throws Exception {
        String uri = "/api/products";
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get(uri))
                .andExpect(status().isOk())
                .andReturn();

        var converted = mapFromJson(mvcResult.getResponse().getContentAsString(), ProductDto[].class);
        return new ArrayList<>(Arrays.asList(converted));
    }

    protected List<OrderDto> getOrders(String uri, String token) throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get(uri)
                .header("Authorization", "Bearer_" + token))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        var converted = mapFromJson(mvcResult.getResponse().getContentAsString(), OrderDto[].class);
        return new ArrayList<>(Arrays.asList(converted));
    }

    protected List<UserDto> getUsers(String token) throws Exception {
        String uri = "/api/users";
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get(uri)
                .header("Authorization", "Bearer_" + token))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
        var converted = mapFromJson(mvcResult.getResponse().getContentAsString(), UserDto[].class);
        return new ArrayList<>(Arrays.asList(converted));
    }
}
